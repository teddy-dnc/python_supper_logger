import os
import uuid
import json
import inspect
import logging.handlers
from datetime import datetime


class LogHandler:
    """
    This class is wrapping Python's logging mechanism.
    All the methods are static.
    The following environment variables are used...
        LOG_LEVEL   0=ALL, 10=DEBUG, 20=INFO, 30=WARNING, 40=ERROR, 50=CRITICAL
        LOG_FOLDER  The folder where the logs are saved. Default value `/var/log`
        LOG_FILE    Log file name. Default value `YYYY-MM-DD HH-MM-SS_uuid.log`
        LOG_CONSOLE_SWITCH  If true (True is default), every message will be printed in console.
    """
    
    # main global members #
    log = logging
    _stopwatches = {}
    # end - main global members #
    
    # style global members #
    default_log_file_name = '{0}_{1}.log'.format(datetime.now().replace(microsecond=0), uuid.uuid4()).replace(':', '-')
    LOG_HANDLER_FORMAT = '{0}({1}) method: {2} \t {3}'
    # end - style global members #
    
    # for more info please checkout: https://docs.python.org/3/library/logging.html#logrecord-attributes
    FORMAT = '%(asctime)s > %(levelname)s\t%(message)s'

    # for more info please checkout: https://docs.python.org/3/library/logging.html#logging-levels
    LOG_LEVEL = os.environ.get('LOG_LEVEL', 'NOTSET')
    LOG_FOLDER = os.environ.get('LOG_FOLDER', '/var/log')
    LOG_CONSOLE_SWITCH = os.environ.get('LOG_CONSOLE_SWITCH', 'True')

    # make sure the file system has the needed folder to start writing logs
    if isinstance(LOG_FOLDER, str) and len(LOG_FOLDER) > 0 and not os.path.exists(LOG_FOLDER):
        os.makedirs(LOG_FOLDER)
        
    # IO global members #
    log_file_name = os.environ.get('LOG_FILE', default_log_file_name)
    LOG_FILE = '{0}/{1}'.format(LOG_FOLDER, log_file_name)
    # end - IO global members #

    # format global members #
    formatter = logging.Formatter(FORMAT)
    handler = logging.handlers.WatchedFileHandler(LOG_FILE)
    handler.setFormatter(formatter)
    _root = logging.getLogger()
    _root.setLevel(LOG_LEVEL)
    _root.addHandler(handler)
    # end - format global members #
    
    # TODO: update to the cloud logger
    if LOG_CONSOLE_SWITCH.upper() in ["TRUE", "1", "YES"]:
        _root.addHandler(logging.StreamHandler())

    # Initializing stack in naive way.
    # Need to be changed to dynamic way, define static variable _stack_size and remove first item only if full.
    _stack_max_size = 10
    _messages_stack = []

    # python levels consts #
    CRITICAL = 50
    FATAL = CRITICAL
    ERROR = 40
    WARNING = 30
    WARN = WARNING
    INFO = 20
    DEBUG = 10
    NOTSET = 0
    # end - python levels consts #

    def __init__(self):
        pass

    @staticmethod
    def get_log_file_path() -> str:
        """
        Get log file full path.
        :return: Log file full path.
        """
        return LogHandler.LOG_FILE

    @staticmethod
    def set_log_level(level: int) -> None:
        """
        Set logging level.
        :param level: Log level.
        :return: None
        """
        if not (isinstance(level, int) and level > 0):
            raise Exception("level must be a positive integer")
        LogHandler._root.setLevel(level=level)

    @staticmethod
    def set_stack_max_size(stack_max_size: int) -> None:
        """
        Set INFO messages stack max size.
        :param stack_max_size: Stack max size
        :return: None
        """
        if not (isinstance(stack_max_size, int) and stack_max_size > 0):
            raise Exception("stack_max_size must be a positive integer")
        LogHandler._stack_max_size = stack_max_size

    @staticmethod
    def _get_file_name(run_level: int = 2) -> str:
        """
        Get the source file name of the calling method.
        :param run_level: Each method in the stack is adding one run level.
        :return: The name of the file that contains the method that called the LogHandler.
        """
        if not (isinstance(run_level, int) and run_level > 0):
            raise Exception("run_level must be a positive integer")
        filename = inspect.stack()[run_level][1]
        first, *middle, last = '/prefix{0}'.format(filename).replace('\\', '/').split('/')
        return last

    @staticmethod
    def _get_line_number(run_level: int = 2) -> str:
        """
        Get the line number in the source file name of the calling method.
        :param run_level: Each method in the stack is adding one run level.
        :return: Line number that called the LogHandler.
        """
        return inspect.stack()[run_level][2]

    @staticmethod
    def _get_method_name(run_level: int = 2) -> str:
        """
        Get the calling method name.
        :param run_level: Each method in the stack is adding one run level.
        :return: Method name that called the LogHandler
        """
        if not (isinstance(run_level, int) and run_level > 0):
            raise Exception("run_level must be a positive integer")
        return inspect.stack()[run_level][3]

    @staticmethod
    def dump_object(object_to_log: object, indent=2) -> str:
        """
        Dump an object and its members to JSON format.
        :param object_to_log: Object to dump.
        :param indent: the indent in the json representation of the object (default to 2 spaces).
        :return: JSON format of the given object.
        """
        if object_to_log is None or not (isinstance(indent, int) and indent > 0):
            raise Exception("object_to_log must be a non None object and indent a positive integer")
        return json.dumps(object_to_log, default=lambda o: o.__dict__, indent=indent)

    @staticmethod
    def _log_message(level: int, message: object, store_log_levels=[INFO]) -> None:
        """
        Write to log. If its an INFO message, it will be added to the message stack that can be retrieved.
        :param level: Logging level 0=NOTSET, 10=DEBUG, 20=INFO, 30=WARNING, 40=ERROR, 50=CRITICAL
        :param message: String or object to log
        :param store_log_levels: list of levels we wish to store this message in memory stack
        :return: None
        """
        if not isinstance(message, str):
            message = LogHandler.dump_object(message)

        if level in store_log_levels:
            LogHandler._add_message(message)

        current_run_level = 3
        LogHandler.log.log(level,
                           LogHandler.LOG_HANDLER_FORMAT.format(LogHandler._get_file_name(run_level=current_run_level),
                                                                LogHandler._get_line_number(run_level=current_run_level),
                                                                LogHandler._get_method_name(run_level=current_run_level),
                                                                message))

    @staticmethod
    def _add_message(message: object) -> None:
        """
        Add message to stack for future retrieving.
        :param message: Message content.
        :return: None
        """
        if len(LogHandler._messages_stack) >= LogHandler._stack_max_size:
            LogHandler._messages_stack.pop(0)
        LogHandler._messages_stack.append(message)

    @staticmethod
    def get_info_messages(number_of_messages: int = 0) -> list:
        """
        Get last INFO message from stack.
        :param number_of_messages: number of messages.
        :return: List of messages.
        """
        reversed_list = LogHandler._messages_stack[::-1]
        if number_of_messages > len(LogHandler._messages_stack) or number_of_messages < 1:
            return reversed_list
        return reversed_list[0:number_of_messages]

    @staticmethod
    def start_stopwatch(message: str) -> str:
        """
        Starts a stopwatch.
        :param message: Message that follows this stopwatch.
        :return: Stopwatch id. Need to pass this id to stop the stopwatch.
        """
        stopwatch_id = str(uuid.uuid4())
        LogHandler._stopwatches[stopwatch_id] = [datetime.now(), message]
        LogHandler._log_message(level=logging.DEBUG, message='Start stopwatch <{0}> \t stopwatch_id is {1}'
                                .format(message, stopwatch_id))
        return stopwatch_id

    @staticmethod
    def stop_stopwatch(stopwatch_id: str, stop_and_remove: bool = True) -> bool:
        """
        Stop a stopwatch.
        :param stopwatch_id: Stopwatch id
        :param stop_and_remove: Default value is True. If you need to use a stopwatch multiple times pass False.
        :return: True if this id exists, False if this id dose not exists.
        """
        if stopwatch_id in LogHandler._stopwatches:
            start_time, msg = LogHandler._stopwatches[stopwatch_id]
            LogHandler._log_message(level=logging.DEBUG, message='Stopwatch <{0}> took {1} \t stopwatch_id is {2}'
                                    .format(msg, datetime.now() - start_time, stopwatch_id))
            if stop_and_remove:
                try:
                    del LogHandler._stopwatches[stopwatch_id]
                except KeyError:
                    LogHandler.warning('Stopwatch id {0} dose not exists.')
            return True
        return False

    @staticmethod
    def log_info_messages(level: int, messages_to_log: list) -> None:
        """
        Logging many messages at once.
        :param level: Logging level 0=NOTSET, 10=DEBUG, 20=INFO, 30=WARNING, 40=ERROR, 50=CRITICAL
        :param messages_to_log: List of messages.
        :return: None
        """
        [LogHandler._log_message(level=level, message=msg) for msg in messages_to_log]

    @staticmethod
    def debug(message: object) -> None:
        """
        Log DEBUG message.
        :param message: Message to log.
        :return: None
        """
        LogHandler._log_message(level=logging.DEBUG, message=message)

    @staticmethod
    def info(message: object) -> None:
        """
        Log INFO message.
        :param message: Message to log.
        :return: None
        """
        LogHandler._log_message(level=logging.INFO, message=message)

    @staticmethod
    def warning(message: object) -> None:
        """
        Log WARNING message.
        :param message: Message to log.
        :return: None
        """
        LogHandler._log_message(level=logging.WARNING, message=message)

    @staticmethod
    def error(message: object) -> None:
        """
        Log ERROR message.
        :param message: Message to log.
        :return: None
        """
        LogHandler._log_message(level=logging.ERROR, message=message)

    @staticmethod
    def critical(message: object) -> None:
        """
        Log CRITICAL message.
        :param message: Message to log.
        :return: None
        """
        LogHandler._log_message(level=logging.CRITICAL, message=message)

    @staticmethod
    def exception(message: str) -> None:
        """
        Log an exception.
        :param message: Message to log, must be string.
        :return: None
        """
        current_run_level = 2
        LogHandler.log.exception(LogHandler.LOG_HANDLER_FORMAT.format(LogHandler._get_file_name(run_level=current_run_level),
                                                                      LogHandler._get_line_number(run_level=current_run_level),
                                                                      LogHandler._get_method_name(run_level=current_run_level),
                                                                      message))
