# library imports
import time
import unittest

# project imports 
from log_handler import LogHandler


class LogHandlerTestCase(unittest.TestCase):
    """
    A Unit-test class to check the main function's performance of 'LogHandler' class
    """

    def test_log_file_creation(self):
        LogHandler.info('First INFO message.')
        self.assertTrue(FileHandler.exists(LogHandler.get_log_file_path()))

    def test_stopwatch(self):
        sw_id = LogHandler.start_stopwatch('Test stopwatch')
        self.assertTrue(LogHandler.stop_stopwatch(stopwatch_id=sw_id, stop_and_remove=False))
        self.assertTrue(LogHandler.stop_stopwatch(stopwatch_id=sw_id))
        self.assertFalse(LogHandler.stop_stopwatch(sw_id))

    def test_info_message_stack(self):
        msg_list = ['m1', 'm2', 'm3']
        self.assertEqual(len(LogHandler.get_info_messages()), 0)
        LogHandler.log_info_messages(20, msg_list)
        self.assertEqual(len(LogHandler.get_info_messages(1)), 1)
        self.assertEqual(len(LogHandler.get_info_messages(2)), 2)
        self.assertEqual(len(LogHandler.get_info_messages()), 3)
        LogHandler.set_stack_max_size(3)
        LogHandler.log_info_messages(20, msg_list)
        self.assertEqual(len(LogHandler.get_info_messages()), 3)

        LogHandler.set_stack_max_size(6)
        LogHandler.log_info_messages(20, msg_list)
        self.assertEqual(len(LogHandler.get_info_messages()), 6)

        LogHandler.log_info_messages(20, msg_list)
        self.assertEqual(len(LogHandler.get_info_messages()), 6)

        LogHandler.set_stack_max_size(10)
        LogHandler.debug('d1')
        LogHandler.info('i1')
        LogHandler.warning('w1')
        LogHandler.critical('c1')
        self.assertEqual(len(LogHandler.get_info_messages()), 7)

    def test_zzz_last_testing(self):
        """
        This method must be ran last
        """
        LogHandler.debug('Last message.')
        lines = FileHandler.read_lines(LogHandler.get_log_file_path())
        # For all previous testings we expect 21 lines in log file.
        self.assertEqual(len(lines), 21)


if __name__ == '__main__':
    unittest.main()
