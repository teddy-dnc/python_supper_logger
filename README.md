# python_supper_logger

# Introduction

A open-source proxy for python logger and other logging libraries with common use of ML\AI developers.

Logger, so basic but so useful. We at DnC-algo notice that the basic python's logger and other packages do not have a basic function we as AI\ML developers need in our loggers... Proud to present our super logger for Python having the functionality each developer needs from his logger nowadays. This is clear that logger is the eyes we have for systems in production and this is right that there are many smart systems monitoring our systems in many aspects. But, in the end, the average developer starts her analysis with the logger, as it's the place most informative and native to start with.
How many time do you write a logger class for your project? as AI \ ML developers this is one of the first things we write before production...

We at DnC-algo decide to proxy the basic python logger packages with the functionality we usully use in our projects.

# Related Work

There are too many loggers projects to count. We decided to develop our logger based on the basic python's logging package.
As logger must to be as resource and dependencies minimal to work smoothness on as much project as possible.

Main ideas for easy configuration of the super logger has been taken from https://docs.python-guide.org/writing/logging/ .

# Installation and Deployment

Add the following packages to your Requerments.txt or install using "pip install" in your system to support remote logging handling.
Add the logger class to your project file tree and you are ready to start.

# Main Features

1. Log Exceptions with separated logic to easy finding in later review
2. Log any object's state as Json or XML for easier

# License

Copyright (c) 2019 DnC-algo Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.